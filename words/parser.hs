

import Data.Tree
import Data.Tree.Zipper
import Data.Maybe
import Data.List
import Utilities
import Debug.Trace

import qualified System.Random 
import qualified Data.Hashable

--data Tree a = Node a [Tree a] deriving (Show,Read,Eq)
--type Crumb a = (Tree a, maybe Int)
--type Zipper a = (Tree a, [Crumb a])




--turns .txt into cat/element tree
category_tree str = 
	let startingTree = Node "TOP" [] 
		in  parse_line_into_tree 0 (lines str) (fromTree startingTree)

--turns word tree into [(cat,[words])]
construct_cat_words ret cats zipp up
	-- | trace ("ccw") False = undefined
	-- | trace ("\n\n" ++ (show $ ret)) False = undefined
	| up = let --we finished processing the children of this node. continue.
		nextSibling = next zipp
		parentZipp = parent zipp
		(process,goingUp) = if isNothing nextSibling then (fromJust parentZipp,True) else (fromJust nextSibling,False)
		in if isNothing parentZipp then (ret,zipp) --we are at TOP level going up
			else if (length cats == 0) then (ret,zipp) --wtf???? how can this ever happen
				else construct_cat_words ret (init cats) process goingUp --recurse, and remove current category
	| (isLeaf zipp) = let --word case, remove it and add the value to (fst ret). Continue to sibling or backto parent
		nextSibling = nextTree (Data.Tree.Zipper.delete zipp)
		(process, goingUp) = if isNothing nextSibling then (fromJust $ parent zipp, True) else (fromJust nextSibling, False)
		join_cats_to_word sCats sWord sRet = if length sCats == 0 then sRet --ignore the top node because it's a dummy... actually don't because we want it
			else join_cats_to_word (init sCats) sWord (special_insert (Data.List.last sCats, [sWord]) sRet)
		newRet = join_cats_to_word cats (label zipp) ret --TODO this needs to add to all cats
		in construct_cat_words newRet cats process goingUp
	| otherwise = --category case. Will always have children 
		construct_cat_words ret (cats ++ [label zipp]) (fromJust $ firstChild zipp) False

--turns tree into [words] that are in the tree
extract_words_from_cat zipp 
	| otherwise = map fst $ reverse_map $ fst $ construct_cat_words [] [] zipp False

--TODO infinite loop bug here
--finds the given category in a tree (first occurrance)
find_element_in_tree element zipp 
	| label zipp == element = Just zipp --found it
	| not $ hasChildren zipp = Nothing
	-- | trace (element ++ (show $  (map label) $ (snd $ until (\(z,lst) -> (fromJust $ lastChild zipp) == z) (\(z,lst) -> (fromJust $ next z,lst ++ [fromJust $ next z])) (fromJust $ firstChild zipp, [fromJust $ firstChild zipp])))) False = undefined
	| otherwise = let 
		initiate = fromJust $ firstChild zipp
		zippedChildren = snd $ until (\(z,lst) -> (fromJust $ lastChild zipp) == z) (\(z,lst) -> (fromJust $ next z,lst ++ [fromJust $ next z])) (initiate, [initiate])
		in foldr (\e acc -> if isJust acc then acc else find_element_in_tree element e) Nothing (zippedChildren)

--gets category n nodes up
--depth 0 means itself counting up
get_category_up n zipp 
	-- | trace (show (label zipp)) False = undefined
	| n == 0 = zipp
	| otherwise = if isJust $ parent zipp then get_category_up (n-1) $ fromJust $ parent zipp else zipp --else case means top node

--constructs the string we want
construct_output_string cat zipp
	-- | trace (cat ++ (show $ (extract_words_from_cat $ get_category_up 5 $ (fromJust $ find_element_in_tree cat zipp)))) False = undefined
	| cat == "TOP" = ""
	| otherwise = let 
		catZipp = fromJust $ find_element_in_tree cat zipp
		goodWordList = extract_words_from_cat catZipp 
		badWordList = take 25 $ shuffle_list (System.Random.mkStdGen $ Data.Hashable.hash cat) $ ((extract_words_from_cat $ get_category_up 5 catZipp) \\ goodWordList)
		--badWordList = ((extract_words_from_cat $ get_category_up 5 catZipp) \\ goodWordList)
		in "CATEGORY\n" ++ cat ++ "\nQUESTIONS\nNONE\nMESSAGE\nNONE\nCORRECT\n" ++ (foldr (\x acc -> acc ++ ((++) x "\n")) [] goodWordList) ++ "INCORRECT\n" ++ (foldr (\x acc -> acc ++ (x++"\n")) [] badWordList)		


test_shuffle_list = do 
	rgen <- System.Random.getStdGen
	print $ shuffle_list rgen ['a'..'z']

main = do 
	contents <- readFile "words.txt"
	--rgen <- System.Random.getStdGen
	let 
		wordTree = toTree (snd (category_tree contents))
		wordZipp = fromTree wordTree
		(catWords,newTree) = construct_cat_words [] [] wordZipp False
		wordCats = reverse_map catWords
		catList = map fst catWords
		outputStringFunction = flip (construct_output_string)
		outputString = foldr (\x acc -> acc ++ x) "" $ map (outputStringFunction wordZipp) catList
		in writeFile "gmwords.txt" outputString 
	

