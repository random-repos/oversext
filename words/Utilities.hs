


module Utilities
( reverse_map,
special_insert,
parse_line_into_tree,
shuffle_list ) where

import Data.List	
import Data.Maybe

import Data.Tree
import Data.Tree.Zipper
import Data.String

import Debug.Trace

import System.Random



--adds tuple into dictionary, unioning values if key already exists
--special_insert :: (Eq k, Eq v) => (k,[v]) -> [(k,[v])] -> [(k,[v])]
special_insert tup dict =
	let concat_me = find (((==) (fst tup)).fst) dict
	in let	
		newDict = 
			if (isJust concat_me) then
				Data.List.delete (fromJust concat_me) dict
			else
				dict
		newTup = 
			if (isJust concat_me) then
				(fst tup, union (snd tup) (snd (fromJust concat_me)))
			else
				tup
		in newDict ++ [newTup]

--special_union :: (Eq k, Eq v) => [(k,[v])] -> [(k,[v])] -> [(k,[v])]
special_union d1 d2  
	| d1 == [] = d2
	| d2 == [] = d1
	| otherwise =
		special_union (special_insert (head d2) d1) (tail d2)

reverse_dict :: (Eq k, Eq v) => [(k,[v])] -> [(v,[k])]
reverse_dict [] = []
reverse_dict (x:xs) = 
	special_union (process_first(x)) (reverse_dict(xs))
	where
		process_first (a,[]) = []
		process_first (a,b) = union ([(head(b), [a])]) (process_first(a, (tail(b))))

reverse_map :: (Eq k, Eq v) => [(k,[v])] -> [(v,[k])]
reverse_map = reverse_dict
			

--[String] -> Tree Full String
strip_tabs (x:xs) = if x == '\t' then strip_tabs $ xs else x:xs
strip_tabs st = st

strip_dash (x:xs) = if x == '-' then xs else x:xs
strip_dash st = st

--parse_line_into_tree:: Int -> [[Char]] -> TreePos Full [Char] -> ([[Char]],TreePos Full [Char])
parse_line_into_tree indent st zipp 
	-- | trace (unlines st ++ "\n\n\n\n\n") False = undefined
	| st == [] = --EOL
		(st,root zipp) --return the results
	-- | trace (show ((tail $ foldr (\_ acc -> acc ++ "\t") "" [0..indent]) `isPrefixOf` (head st))) False = undefined
	| (strip_tabs $ head st) == "" = parse_line_into_tree indent (tail st) zipp --empty line case
	| ((tail $ foldr (\_ acc -> acc ++ "\t") "" [0..indent]) `isPrefixOf` (head st)) = 
		let --we are the the level we want 
			newLoc = Data.Tree.Zipper.insert (Node (strip_dash $ strip_tabs $ head st) []) (children zipp) --add current node		
			(retSt, retZipp) = parse_line_into_tree (indent+1) (tail st) newLoc --attempt to add children
			in parse_line_into_tree indent retSt retZipp --add next node
	| parent zipp == Nothing = --this should never happen
		(st,root zipp)
	| otherwise =  --we raen't at the level we want so go back a level
		(st, fromJust $ parent zipp)


remove_at_index index lst = 
	let subfunc (i,r) x = if i == index then (i+1,r) else (i+1, r ++ [x])
		in snd $ foldl subfunc (0,[]) lst

--shuffle_list::(RandomGen g) => g -> [k] -> [k]
shuffle_list rgen lst = 
	let random_index sub1rgen indices 
		| length indices == 0 = []
		| otherwise = 
			let 
				(notModIndex,sub2rgen) = random sub1rgen
				useIndex = notModIndex `mod` (length indices) :: Int
				in [lst !! (indices !! useIndex)] ++ random_index sub2rgen (remove_at_index useIndex indices)
		--in foldr (\x acc -> acc ++ [(lst !! x)]) [] (random_index rgen [0..(length lst - 1)])
		in random_index rgen [0..(length lst - 1)]

